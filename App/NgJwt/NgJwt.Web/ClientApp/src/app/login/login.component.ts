import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidLogin: boolean;

  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
  }

  login(form: NgForm) {
    console.log("login method");

    let credentials = JSON.stringify(form.value);

    this.http.post("https://localhost:44346/api/authorization/login", credentials, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).subscribe(response => {
      let token = (<any>response).token;
      localStorage.setItem("jwt", token);
      this.invalidLogin = false;

      this.router.navigate(["/"]);
    }, err => {
      console.log(err);
      this.invalidLogin = true;
    });
  }

  isLoggedIn() {
    if (localStorage.getItem("jwt")) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem("jwt");
  }

}
