import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';


@Injectable()
export class AuthGuardsService implements CanActivate {

  constructor(private router: Router) { }

  canActivate() {
    var token = localStorage.getItem("jwt");

    if (tokenNotExpired("jwt")) {
      return true;
    }

    this.router.navigate(["login"]);
    return false;
    
  }

}
