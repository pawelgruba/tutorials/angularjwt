import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()

export class AuthService {

  constructor() {
    console.log('AuthService initialization');}

  public getToken(): string {
    return localStorage.getItem('jwt');
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();

    return tokenNotExpired(null, token);
  }

}
